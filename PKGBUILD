# Maintainer: Ray Sherwin <slick517d@gmail.com>
# Author: graysky <therealgraysky AT protonmail DOT com>

pkgname=raspberrypi-utils
pkgver=20250304
pkgrel=1
_commit=660bba39805585fb39d37d49b8268073932696bb
pkgdesc="Legacy scripts and simple applications for Raspberry Pi"
arch=('aarch64' 'armv7h')
url="https://github.com/raspberrypi/utils"
license=('custom')
makedepends=('cmake' )
depends=('dtc')
replaces=('raspberrypi-firmware' 'raspberrypi-userland-aarch64')
conflicts=('raspberrypi-firmware' 'raspberrypi-userland-aarch64')
options=(!strip)
source=("utils-$pkgver-$pkgrel-${_commit:0:10}.tar.gz::https://github.com/raspberrypi/utils/archive/$_commit.tar.gz"
         10-raspberrypi-utils.rules
)

md5sums=('471ef5a86aba76d99b3e21a9982a2605'
         'a0423d9850cc56a15967e103c351c335')

build() {
unset CFLAGS CXXFLAGS LDFLAGS
  cd "utils-$_commit"
  CFLAGS+=" -O2 -pipe -fstack-protector-strong -fno-plt -fexceptions -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security -fstack-clash-protection"
  CXXFLAGS+="${CFLAGS} -Wp,-D_GLIBCXX_ASSERTIONS"
  LDFLAGS+="-Wl,-O1 -Wl,--sort-common -Wl,--as-needed -Wl,-z,relro -Wl,-z,now"
  cmake . -DCMAKE_INSTALL_PREFIX=/usr
  make
}

package() {
  cd "utils-${_commit}"
  make install DESTDIR="$pkgdir"
  install -Dm0644 LICENCE -t "$pkgdir/usr/share/licenses/$pkgname"

  # setup permissions on video group for /usr/bin/vcgencmd
  install -Dm0644 ../10-raspberrypi-utils.rules "$pkgdir/usr/lib/udev/rules.d/10-raspberrypi-utils.rules"
}
